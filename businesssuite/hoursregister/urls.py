from django.urls import path
from django.conf import settings
from django.conf.urls import include, url

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('team', views.team, name='team'),
    path('leave', views.leave, name='leave'),
    path('leave/create', views.get_leave_input, name='add_leave'),
    path('leave/<int:pk>/update/', views.LeaveUpdate.as_view(), name='update_leave'),
    path('leave/<int:pk>/delete/', views.LeaveDelete.as_view(), name='delete_leave'),
    path('leave/<int:id>/accept/', views.accept_leave, name='accept_leave'),
    path('leave/<int:id>/reject/', views.reject_leave, name='reject_leave'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns