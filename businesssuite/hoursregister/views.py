from django.shortcuts import render, redirect
from .models import Leave, Team
from businesstime import BusinessTime
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from decimal import Decimal

from .forms import LeaveForm

def index(request):
    """
    View function for home page
    """

    leaves = Leave.objects.filter(user__exact=request.user)

    leaves_pending = Leave.objects.filter(user__exact=request.user, status__exact='P')
    leaves_accepted = Leave.objects.filter(user__exact=request.user, status__exact='A')
    leaves_accepted_recent = Leave.objects.filter(user__exact=request.user, status__exact='A')[:5]
    leaves_rejected = Leave.objects.filter(user__exact=request.user, status__exact='R')
    leaves_rejected_recent = Leave.objects.filter(user__exact=request.user, status__exact='R')[:5]
    leaves_pending_count = leaves_pending.count()
    leaves_accepted_count = leaves_accepted.count()
    leaves_rejected_count = leaves_rejected.count()

    users_in_team = User.objects.filter(team__members=request.user)

    leaves_pending_team = Leave.objects.filter(user__in=users_in_team, status__exact='P').exclude(user=request.user)
    leaves_pending_team_count = Leave.objects.filter(user__in=users_in_team, status__exact='P').exclude(user=request.user).count()

    return render(
        request,
        'index.html',
        context={'leaves':leaves,
                 'leaves_pending': leaves_pending, 'leaves_pending_count': leaves_pending_count,
                 'leaves_accepted': leaves_accepted, 'leaves_accepted_count': leaves_accepted_count, 'leaves_accepted_recent': leaves_accepted_recent,
                 'leaves_rejected': leaves_rejected, 'leaves_rejected_count': leaves_rejected_count, 'leaves_rejected_recent': leaves_rejected_recent,
                 'leaves_pending_team': leaves_pending_team, 'leaves_pending_team_count': leaves_pending_team_count,
                 'users_in_team': users_in_team,
                 }
    )

def team(request):
    """
    View function for team page
    """
    teams = Team.objects.filter(members__exact=request.user)
    users_in_team = User.objects.filter(team__members=request.user)

    return render(
        request,
        'team.html',
        context={'teams' : teams,
                 }
    )

def leave(request):
    """
    View function for leave overview page
    """
    leaves = Leave.objects.filter(user__exact=request.user)

    leaves_pending = Leave.objects.filter(user__exact=request.user, status__exact='P')
    leaves_accepted = Leave.objects.filter(user__exact=request.user, status__exact='A')
    leaves_rejected = Leave.objects.filter(user__exact=request.user, status__exact='R')
    leaves_pending_count = leaves_pending.count()
    leaves_accepted_count = leaves_accepted.count()
    leaves_rejected_count = leaves_rejected.count()

    users_in_team = User.objects.filter(team__members=request.user)

    leaves_pending_team = Leave.objects.filter(user__in=users_in_team, status__exact='P').exclude(user=request.user)
    leaves_pending_team_count = Leave.objects.filter(user__in=users_in_team, status__exact='P').exclude(user=request.user).count()

    return render(
        request,
        'leave_overview.html',
        context={'leaves': leaves,
                 'leaves_accepted': leaves_accepted,
                 'leaves_rejected': leaves_rejected,
                 'leaves_pending_team': leaves_pending_team,
                 'users_in_team': users_in_team,
                 }
    )

@login_required
def get_leave_input(request):
    if request.method == "POST":
        form = LeaveForm(request.POST)
        if form.is_valid():
            # commit=False means the form doesn't save at this time.
            # commit defaults to True which means it normally saves.
            leave = form.save(commit=False)
            leave.user = request.user
            balance = leave.user.profile.balance

            #calculate business hours
            bt = BusinessTime()
            workingdays = bt.businesstimedelta(leave.start_date, leave.end_date)
            workinghours = workingdays.total_seconds() // 3600

            if balance < workinghours:
                messages.error(request, 'Insufficient balance ({} hours)'.format(balance))
                return render(request, 'leave_form.html', {'form': form})
            else:
                leave.total_hours = workinghours
                total_hours = Decimal.from_float(leave.total_hours)

                new_balance = balance - total_hours
                leave.user.profile.balance = new_balance
                leave.user.profile.save()
                leave.save()
                messages.success(request, 'Leave added')
                return redirect('/hours/')
    else:
        form = LeaveForm()
        return render(request, 'leave_form.html', {'form': form})

@login_required
def accept_leave(request, id):
    if Leave.set_status(request, id):
        leave = Leave.objects.get(id=id)
        leave.status = 'A'
        leave.handled_by = request.user
        leave.save()

        messages.success(request, 'Leave accepted')
        return redirect('/hours/')
    else:
        messages.error(request, 'Form submission failed')

@login_required
def reject_leave(request, id):
    if Leave.set_status(request, id):
        leave = Leave.objects.get(id=id)
        leave.status = 'R'
        leave.save()

        new_balance = leave.user.profile.balance + leave.total_hours
        leave.user.profile.balance = new_balance
        leave.user.profile.save()

        messages.success(request, 'Leave rejected')
        return redirect('/hours/')
    else:
        messages.error(request, 'Form submission failed')

class LeaveUpdate(SuccessMessageMixin, UpdateView):
    model = Leave
    form_class = LeaveForm
    template_name = 'leave_form.html'
    success_message = 'Leave updated'


class LeaveDelete(DeleteView):
    model = Leave
    template_name = 'leave_confirm_delete.html'
    success_message = 'Leave deleted'
    success_url = reverse_lazy('index')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(LeaveDelete, self).delete(request, *args, **kwargs)