from django.apps import AppConfig


class HoursregisterConfig(AppConfig):
    name = 'hoursregister'

    def ready(self):
        import hoursregister.signals