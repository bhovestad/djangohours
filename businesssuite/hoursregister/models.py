from django.db import models
from django.conf import settings
from django.urls import reverse


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    balance = models.DecimalField(max_digits=5,decimal_places=2,editable=False,default=400)

class Leave(models.Model):

    class Meta:
        permissions = (
            ("handle_leave", "Can change the status of a leave"),
        )

    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    total_hours = models.DecimalField(max_digits=5,decimal_places=2,editable=False)
    note = models.TextField()
    entire_day = True

    PENDING = 'P'
    ACCEPTED = 'A'
    REJECTED = 'R'
    APPLICATION_STATUS = (
        (PENDING, 'Pending'),
        (ACCEPTED, 'Accepted'),
        (REJECTED, 'Rejected')
    )

    status = models.CharField(
        max_length=1,
        choices=APPLICATION_STATUS,
        default=PENDING
    )

    type = models.ForeignKey(
        'LeaveType',
        on_delete=models.CASCADE
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='user'
    )

    handled_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='handled_by',
        null=True
    )

    def set_status(self, status):
        self.status = status
        return self.status

    def __str__(self):
        return '{0} by {1}'.format(self.type,self.user)

    def get_absolute_url(self):
        """
        Returns the url to access a detail record for this application.
        """
        return reverse('update_leave', args=[str(self.id)])

class Holiday(models.Model):
    name = models.CharField(max_length=40)
    start_date = models.DateField()
    end_date = models.DateField()
    active = models.BooleanField(default=True)
    repeat_every_year = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class LeaveType(models.Model):
    name = models.CharField(max_length=40)
    active = models.BooleanField(default=True)
    show_on_calender = models.BooleanField(default=True)

    def __str__(self):
        return self.name

#Skills that belong to a user
class Skill(models.Model):
    name = models.CharField(max_length=250)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name

class Team(models.Model):
    name = models.CharField(max_length=40)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name

