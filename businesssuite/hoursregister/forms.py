from bootstrap3_datetime.widgets import DateTimePicker
from django.forms import ModelForm, DateTimeInput
from .models import Leave

class LeaveForm(ModelForm):

    class Meta:
        model = Leave
        fields = ("start_date",
                  "end_date",
                  "note",
                  "type",)
        widgets = {
            'start_date': DateTimePicker(options={"format": "YYYY-MM-DD H:MM"}, attrs={'class': 'date_field'},),
            'end_date': DateTimePicker(options={"format": "YYYY-MM-DD H:MM"}, attrs={'class': 'date_field'},)
        }