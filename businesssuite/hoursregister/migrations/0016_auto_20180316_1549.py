# Generated by Django 2.0.2 on 2018-03-16 15:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hoursregister', '0015_auto_20180304_1914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='balance',
            field=models.DecimalField(decimal_places=2, default=400, editable=False, max_digits=5),
        ),
    ]
