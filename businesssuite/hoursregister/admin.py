from django.contrib import admin
from .models import Leave, LeaveType, Holiday, Skill, Team

@admin.register(Leave)
class LeaveAdmin(admin.ModelAdmin):
    list_display = ('user', 'start_date', 'end_date', 'total_hours', 'status')
    list_filter = ('status', 'start_date')
    fields = ['user', 'type', ('start_date', 'end_date'), 'note', 'status']

admin.site.register(LeaveType)
admin.site.register(Holiday)
admin.site.register(Skill)
admin.site.register(Team)